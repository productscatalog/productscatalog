package br.com.tlf.fasttrack.productscatalog;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.tlf.fasttrack.productscatalog.domain.Product;

@SpringBootTest
class ProductsCatalogApplicationTests {

	@Test
	void contextLoads() {
		
		Product product = new Product();
		
		product.setDescription("Teste");
		product.setName("Teste");
		product.setPrice(200);
		
		assertThat(product).isNotNull();
	}

}
