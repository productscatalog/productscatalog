DROP TABLE IF EXISTS products;

CREATE TABLE products (
  id IDENTITY NOT NULL  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  description VARCHAR(250) NOT NULL,
  price NUMBER DEFAULT NULL
);

INSERT INTO products (name, description, price) VALUES
  ('Mouse Microsoft', 'Mouse wireless', 199.90),
  ('Teclado Logitech', 'Teclado ABNT 2', 350.75),
  ('Monitor', 'Monitor Benk 17 polegadas', 155.70);