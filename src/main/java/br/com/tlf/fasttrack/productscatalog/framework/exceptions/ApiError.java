package br.com.tlf.fasttrack.productscatalog.framework.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Modelo de erro")
public class ApiError  {
	
	@JsonProperty("status_code")
	private int statusCode;
	
	@JsonProperty("message")
	private String message;

	public int getstatusCode() {
		return statusCode;
	}


	public String getMessage() {
		return message;
	}

	
	public ApiError(int statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}	
	
}
