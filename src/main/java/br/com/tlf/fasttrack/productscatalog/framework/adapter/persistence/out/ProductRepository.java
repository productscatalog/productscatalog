package br.com.tlf.fasttrack.productscatalog.framework.adapter.persistence.out;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.tlf.fasttrack.productscatalog.framework.adapter.persistence.out.entity.ProductsDTO;

@Repository
public interface ProductRepository extends JpaRepository<ProductsDTO, Long>, JpaSpecificationExecutor<ProductsDTO> {

	

}
