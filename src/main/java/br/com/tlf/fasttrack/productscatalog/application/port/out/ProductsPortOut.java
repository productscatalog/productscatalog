package br.com.tlf.fasttrack.productscatalog.application.port.out;

import java.util.List;
import java.util.Optional;

import br.com.tlf.fasttrack.productscatalog.domain.Product;
import br.com.tlf.fasttrack.productscatalog.domain.ProductResponse;

public interface ProductsPortOut {
	
	ProductResponse getProductById(int id);
	
	List<ProductResponse> getProducts();
	
	void deleteProductById(int id);
	
	ProductResponse updateProductById(int id, Product product);
	
	ProductResponse insertProduct(Product product);
	
	List<ProductResponse> searchProducts(Optional<String> q, Optional<Double> minPrice, Optional<Double> maxPrice);

}
