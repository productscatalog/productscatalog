package br.com.tlf.fasttrack.productscatalog.framework.exceptions;

import javax.ws.rs.NotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class ControllerAdvice {

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<ApiError> handleException(MethodArgumentTypeMismatchException ex) {
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), "Invalid Arguments");

		return ResponseEntity.badRequest().body(apiError);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ApiError> handleException(MethodArgumentNotValidException ex) {
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), "Invalid or Unfilled Arguments ");

		return ResponseEntity.badRequest().body(apiError);
	}
	
	
	

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ApiError> handleException(NotFoundException ex) {
		ApiError apiError = new ApiError(HttpStatus.NOT_FOUND.value(), ex.getMessage());

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(apiError);
	}
	
	
	
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<ApiError> handleException(HttpMessageNotReadableException ex) {
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), "Invalid or Unfilled Arguments ");

		return ResponseEntity.badRequest().body(apiError);
	}
	
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ApiError> handleException(Exception ex) {
		ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "INTERNAL SERVER ERROR");

		return ResponseEntity.badRequest().body(apiError);
	}
}
