package br.com.tlf.fasttrack.productscatalog.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Modelo de Produto
 */
@ApiModel(description = "Modelo de Produto")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-09-07T21:26:19.578187800-03:00[America/Sao_Paulo]")

public class Product   {
  @JsonProperty("name")
  private String name;

  @JsonProperty("description")
  private String description;

  @JsonProperty("price")
  @Positive
  private double price;

  public Product name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Nome do Produto
   * @return name
  */
  @ApiModelProperty(example = "Mouse", required = true, value = "Nome do Produto")
  @NotNull
  @NotBlank


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Product description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Descrição do produto
   * @return description
  */
  @ApiModelProperty(example = "Mouse ótico Microsoft", required = true, value = "Descrição do produto")
  @NotNull
  @NotBlank


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Product price(double price) {
    this.price = price;
    return this;
  }

  /**
   * Preço do produto
   * @return price
  */
  @ApiModelProperty(example = "59.99", required = true, value = "Preço do produto")
  @NotNull

  @Valid
  @Positive

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }  
}

