package br.com.tlf.fasttrack.productscatalog.framework.adapter.persistence.out.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import br.com.tlf.fasttrack.productscatalog.application.port.out.ProductsPortOut;
import br.com.tlf.fasttrack.productscatalog.domain.Product;
import br.com.tlf.fasttrack.productscatalog.domain.ProductResponse;
import br.com.tlf.fasttrack.productscatalog.framework.adapter.persistence.out.ProductRepository;
import br.com.tlf.fasttrack.productscatalog.framework.adapter.persistence.out.entity.ProductsDTO;
import br.com.tlf.fasttrack.productscatalog.framework.adapter.persistence.out.entity.ProductsMetaModel;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Component
public class ProductsAdapter implements ProductsPortOut {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public ProductResponse getProductById(int id) {
		Optional<ProductsDTO> productJpa = productRepository.findById((long) id);

		if (!productJpa.isPresent()) {
			return null;
		}

		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

		mapperFactory.classMap(ProductsDTO.class, ProductResponse.class);
		MapperFacade mapper = mapperFactory.getMapperFacade();

		return mapper.map(productJpa.get(), ProductResponse.class);

	}

	@Override
	public List<ProductResponse> getProducts() {

		List<ProductsDTO> productsJpa = productRepository.findAll();

		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(ProductsDTO.class, ProductResponse.class);
		MapperFacade mapper = mapperFactory.getMapperFacade();

		return mapper.mapAsList(productsJpa, ProductResponse.class);

	}

	@Override
	public void deleteProductById(int id) {

		productRepository.deleteById((long) id);

	}

	@Override
	public ProductResponse updateProductById(int id, Product product) {

		ProductsDTO productToBeUpdated = productRepository.getById((long) id);

		productToBeUpdated.setName(product.getName());
		productToBeUpdated.setDescription(product.getDescription());
		productToBeUpdated.setPrice(product.getPrice());

		ProductsDTO updatedProduct = productRepository.save(productToBeUpdated);

		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(ProductsDTO.class, ProductResponse.class);
		MapperFacade mapper = mapperFactory.getMapperFacade();

		return mapper.map(updatedProduct, ProductResponse.class);

	}

	@Override
	public ProductResponse insertProduct(Product product) {

		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(Product.class, ProductsDTO.class);
		MapperFacade mapper = mapperFactory.getMapperFacade();

		ProductsDTO insertedProduct = productRepository.save(mapper.map(product, ProductsDTO.class));

		mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(ProductsDTO.class, ProductResponse.class);
		mapper = mapperFactory.getMapperFacade();

		return mapper.map(insertedProduct, ProductResponse.class);
	}

	@Override
	public List<ProductResponse> searchProducts(Optional<String> q, Optional<Double> minPrice, Optional<Double> maxPrice) {

		List<ProductsDTO> productsJpa = productRepository
				.findAll(nameOrDescriptionLike(q).and(priceMin(minPrice)).and(priceMax(maxPrice)));

		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(ProductsDTO.class, ProductResponse.class);
		MapperFacade mapper = mapperFactory.getMapperFacade();

		return mapper.mapAsList(productsJpa, ProductResponse.class);
	}

	private Specification<ProductsDTO> nameOrDescriptionLike(Optional<String> nameOrDescription) {

		if (!nameOrDescription.isPresent()) {
			return Specification.where(null);
		}
		
		return (root, query, criteriaBuilder) -> criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.upper(root.get(ProductsMetaModel.NAME)) ,
				"%" + nameOrDescription.get().toUpperCase() + "%"), criteriaBuilder.like(criteriaBuilder.upper(root.get(ProductsMetaModel.DESCRIPTION)),
						"%" + nameOrDescription.get().toUpperCase() + "%"));
	}

	private Specification<ProductsDTO> priceMin(Optional<Double> price) {
		if (!price.isPresent()) {
			return Specification.where(null);
		}

		return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(ProductsMetaModel.PRICE),
				price.get());
	}

	private Specification<ProductsDTO> priceMax(Optional<Double> price) {
		if (!price.isPresent()) {
			return Specification.where(null);
		}

		return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(ProductsMetaModel.PRICE),
				price.get());		
		
	}

}
