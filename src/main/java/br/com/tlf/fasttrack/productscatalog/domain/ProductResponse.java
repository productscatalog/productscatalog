package br.com.tlf.fasttrack.productscatalog.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Modelo de retorno, contemplando a lista de produtos
 */
@ApiModel(description = "Modelo de retorno, contemplando a lista de produtos")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-09-07T21:26:19.578187800-03:00[America/Sao_Paulo]")

public class ProductResponse   {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("name")
  private String name;

  @JsonProperty("description")
  private String description;

  @JsonProperty("price")
  private double price;

  public ProductResponse id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Id do produto
   * @return id
  */
  @ApiModelProperty(example = "1", value = "Id do produto")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public ProductResponse name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Nome do Produto
   * @return name
  */
  @ApiModelProperty(example = "Mouse", required = true, value = "Nome do Produto")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ProductResponse description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Descrição do produto
   * @return description
  */
  @ApiModelProperty(example = "Mouse ótico Microsoft", required = true, value = "Descrição do produto")
  @NotNull


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ProductResponse price(double price) {
    this.price = price;
    return this;
  }

  /**
   * Preço do produto
   * @return price
  */
  @ApiModelProperty(example = "59.99", required = true, value = "Preço do produto")
  @NotNull

  @Valid

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }


}

