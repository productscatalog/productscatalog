package br.com.tlf.fasttrack.productscatalog.application.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.tlf.fasttrack.productscatalog.application.port.in.ProductsUseCase;
import br.com.tlf.fasttrack.productscatalog.application.port.out.ProductsPortOut;
import br.com.tlf.fasttrack.productscatalog.domain.Product;
import br.com.tlf.fasttrack.productscatalog.domain.ProductResponse;

@Service
public class ProductsService implements ProductsUseCase {

	@Autowired
	private ProductsPortOut productsPortOut;

	@Override
	public ProductResponse productsIdGet(int id) {
		return productsPortOut.getProductById(id);
	}

	@Override
	public List<ProductResponse> productsGet() {
		return productsPortOut.getProducts();
	}

	@Override
	public void productsIdDelete(int id) {
		productsPortOut.deleteProductById(id);
	}

	@Override
	public ProductResponse updateProductById(int id, Product product) {
		return productsPortOut.updateProductById(id, product);
	}

	@Override
	public ProductResponse insertProduct(Product product) {
		return productsPortOut.insertProduct(product);
	}

	@Override
	public List<ProductResponse> searchProducts(Optional<String> q, Optional<Double> minPrice, Optional<Double> maxPrice) {
		return productsPortOut.searchProducts(q, minPrice, maxPrice);
	}

}
