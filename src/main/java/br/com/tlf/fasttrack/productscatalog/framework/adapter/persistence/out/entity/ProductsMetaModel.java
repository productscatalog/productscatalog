package br.com.tlf.fasttrack.productscatalog.framework.adapter.persistence.out.entity;

import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ProductsDTO.class)
public abstract class ProductsMetaModel {

	public static final String PRICE = "price";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	
	private ProductsMetaModel() {
		throw new IllegalStateException("ProductsMetaModel class");
	}

}
