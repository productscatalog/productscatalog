package br.com.tlf.fasttrack.productscatalog.framework.adapter.in.rest;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tlf.fasttrack.productscatalog.application.port.in.ProductsUseCase;
import br.com.tlf.fasttrack.productscatalog.domain.Product;
import br.com.tlf.fasttrack.productscatalog.domain.ProductResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-09-07T21:26:19.578187800-03:00[America/Sao_Paulo]")

@RestController
@RequestMapping("${openapi.productsCatalog.base-path:}")
@Validated
public class ProductsApiController {

	@Autowired
	private ProductsUseCase productsUseCase;

	/**
	 * GET /products/{id} : Busca Produto Busca produto pelo ID cadastrado
	 *
	 * @param id Product Id (required)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Busca Produto", nickname = "productsIdGet", notes = "Busca produto pelo ID cadastrado", response = ProductResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ProductResponse.class) })
	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductResponse> productsIdGet(
			@ApiParam(value = "Product Id", required = true) @PathVariable("id") Integer id) {

		ProductResponse productResponse;

		try {
			productResponse = productsUseCase.productsIdGet(id);

			if (productResponse == null) {
				throw new NotFoundException();
			}

		} catch (NoSuchElementException | NotFoundException e) {

			throw new NotFoundException("Resource Not Found");
		}

		return ResponseEntity.ok().body(productResponse);
	}

	/**
	 * GET /products : Lista Produtos Listagem de todos os produtos cadastrados
	 *
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Lista Produtos", nickname = "productsGet", notes = "Listagem de todos os produtos cadastrados", response = ProductResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ProductResponse.class, responseContainer = "List") })
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<List<ProductResponse>> productsGet() {
		return ResponseEntity.ok().body(productsUseCase.productsGet());
	}

	/**
	 * DELETE /products/{id} : Exclui produto Exclui produto pelo ID
	 *
	 * @param id Product Id (required)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Exclui produto", nickname = "productsIdDelete", notes = "Exclui produto pelo ID")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	@RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> productsIdDelete(
			@ApiParam(value = "Product Id", required = true) @PathVariable("id") Integer id) {

		try {
			productsUseCase.productsIdDelete(id);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException("Resource Not Found");
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * PUT /products/{id} : Atualiza produto Atualiza o produto pelo ID
	 *
	 * @param id       Product Id (required)
	 * @param products Modelo de entrada de produto (required)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Atualiza produto", nickname = "productsIdPut", notes = "Atualiza o produto pelo ID")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	@RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
	public ResponseEntity<ProductResponse> productsIdPut(
			@ApiParam(value = "Product Id", required = true) @PathVariable("id") Integer id,
			@ApiParam(value = "Modelo de entrada de produto", required = true) @Valid @RequestBody Product products) {

		ProductResponse productResponse = null;

		try {
			productResponse = productsUseCase.updateProductById(id, products);
		} catch (EntityNotFoundException e) {
			throw new NotFoundException("Resource Not Found");
		}

		return ResponseEntity.ok().body(productResponse);
	}

	/**
	 * POST /products : Inclusão de Produto Esta operação inclui um novo produto
	 *
	 * @param products Modelo de entrada de produto (required)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Inclusão de Produto", nickname = "productsPost", notes = "Esta operação inclui um novo produto", response = ProductResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ProductResponse.class) })
	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public ResponseEntity<ProductResponse> productsPost(
			@ApiParam(value = "Modelo de entrada de produto", required = true) @Valid @RequestBody Product products) {
		ProductResponse productResponse = null;

		productResponse = productsUseCase.insertProduct(products);

		return ResponseEntity.created(null).body(productResponse);
	}

	/**
	 * GET /products/search : Busca Produto Realiza a busca por produto levando em
	 * conta os criterios definidos
	 *
	 * @param q        Valor (optional)
	 * @param minPrice Min Price (optional)
	 * @param maxPrice Max Price (optional)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Busca Produto", nickname = "productsSearchGet", notes = "Realiza a busca por produto levando em conta os criterios definidos", response = ProductResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ProductResponse.class, responseContainer = "List") })
	@RequestMapping(value = "/products/search", method = RequestMethod.GET)
	public ResponseEntity<List<ProductResponse>> productsSearchGet(
			@ApiParam(value = "Valor") @Valid @RequestParam(value = "q", required = false) Optional<String> q,
			@ApiParam(value = "Min Price") @Valid @RequestParam(value = "min_price", required = false) Optional<Double> minPrice,
			@ApiParam(value = "Max Price") @Valid @RequestParam(value = "max_price", required = false) Optional<Double> maxPrice) {

		return ResponseEntity.ok().body(productsUseCase.searchProducts(q, minPrice, maxPrice));
	}

}
